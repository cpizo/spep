CREATE TABLE tb_persona (
    id_persona INT AUTO_INCREMENT PRIMARY KEY,
    apellido VARCHAR(100),
    direccion VARCHAR(100),
    nombre VARCHAR(100),
    telefono VARCHAR(100)
);

CREATE TABLE tb_rol (
    id_rol INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100)
);

CREATE TABLE tb_equipo_trabajo (
    id_equipo_trabajo INT AUTO_INCREMENT PRIMARY KEY,
    id_persona INT,
    id_rol INT,
    FOREIGN KEY (id_persona) REFERENCES tb_persona (id_persona),
    FOREIGN KEY (id_rol) REFERENCES tb_rol (id_rol)
);

CREATE TABLE tb_historia_usuario (
    id_historia_usuario INT AUTO_INCREMENT PRIMARY KEY,
    criterio_aceptacion VARCHAR(100),
    nombre VARCHAR(100),
    funsionalidad VARCHAR(100),
    razon VARCHAR(100),
    id_persona INT,
    FOREIGN KEY (id_persona) REFERENCES tb_persona (id_persona),
    rol VARCHAR(100)
);

CREATE TABLE tb_productbacklog (
    id_productbacklog INT AUTO_INCREMENT PRIMARY KEY,
    complejidad VARCHAR(100),
    prioridad VARCHAR(100),
    id_historia_usuario INT,
    FOREIGN KEY (id_historia_usuario) REFERENCES tb_historia_usuario (id_historia_usuario)
);

CREATE TABLE tb_proyecto (
    id_proyecto INT AUTO_INCREMENT PRIMARY KEY,
    duracion VARCHAR(100),
    id_equipo_trabajo INT,
    FOREIGN KEY (id_equipo_trabajo) REFERENCES tb_equipo_trabajo (id_equipo_trabajo),
    historia_max VARCHAR(100),
    historia_min VARCHAR(100),
    nombre VARCHAR(100),
    peso VARCHAR(100),
    pivote VARCHAR(100),
    tiempo VARCHAR(100)
);



CREATE TABLE tb_user (
    id_user INT AUTO_INCREMENT PRIMARY KEY,
    correo VARCHAR(100),
    pass VARCHAR(100),
    rol VARCHAR(100)
);



