package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.model.ProductbacklogModel;
import uniajc.edu.spep.repository.ProducbacklogRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ProductbacklogController {

    @Autowired
    ProducbacklogRepository producbacklogRepository;

    @GetMapping("/producbacklog")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<ProductbacklogModel> getAllProducbacklog() { return producbacklogRepository.findAll();}

    @GetMapping("/producbacklog/proyecto/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<ProductbacklogModel> fimdNew(@PathVariable(value = "id") Long Id) {
        return producbacklogRepository.fimdId_poryect(Id);
    }

    @PostMapping("/producbacklog")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ProductbacklogModel createProducbacklog(@Valid @RequestBody ProductbacklogModel productbacklogModel) {
        return producbacklogRepository.save(productbacklogModel);
    }

    @GetMapping("/producbacklog/{id_productbacklog}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ProductbacklogModel getProducbacklogById(@PathVariable(value = "id_productbacklog") Long id_productbacklog) {
        return producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));
    }

    @PutMapping("/producbacklog/{id_productbacklog}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ProductbacklogModel updateProducbacklog(@PathVariable(value = "id_productbacklog") Long id_productbacklog,
                                                   @Valid @RequestBody ProductbacklogModel producbacklogDetails) {

        ProductbacklogModel productbacklogModel = producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));

        productbacklogModel.setComplejidad(producbacklogDetails.getComplejidad());
        productbacklogModel.setPrioridad(producbacklogDetails.getPrioridad());
        productbacklogModel.setId_historia_usuario(producbacklogDetails.getId_historia_usuario());
        productbacklogModel.setId_poryect(producbacklogDetails.getId_poryect());

        ProductbacklogModel updatedProducbacklog = producbacklogRepository.save(productbacklogModel);
        return updatedProducbacklog;
    }

    @DeleteMapping("/producbacklog/{id_productbacklog}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteProducbacklog(@PathVariable(value = "id_productbacklog") Long id_productbacklog) {
        ProductbacklogModel updatedProducbacklog = producbacklogRepository.findById(id_productbacklog)
                .orElseThrow(() -> new ResourceNotFoundException("Producbacklog", "id_productbacklog", id_productbacklog));

        producbacklogRepository.delete(updatedProducbacklog);

        return ResponseEntity.ok().build();
    }
}
