package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.Equipo_trabajoModel;
import uniajc.edu.spep.model.Historia_usuarioModel;
import uniajc.edu.spep.repository.HistoriaUsuarioRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class HistoriaUsuarioController {

    @Autowired
    HistoriaUsuarioRepository historiaUsuarioRepository;


    @GetMapping("/historia")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<Historia_usuarioModel> getAllHistoriaUsuario() { return historiaUsuarioRepository.findAll();}

    @GetMapping("/historia/proyecto/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<Historia_usuarioModel> fimdNew(@PathVariable(value = "id") Long Id) {
        return historiaUsuarioRepository.fimdId_poryect(Id);
    }

    @PostMapping("/historia")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Historia_usuarioModel historia_usuarioModel(@Valid @RequestBody Historia_usuarioModel historia_usuarioModel) {
        return historiaUsuarioRepository.save(historia_usuarioModel);
    }


    @GetMapping("/historia/{id_historia_usuario}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Historia_usuarioModel getHistoriaById(@PathVariable(value = "id_historia_usuario") Long id_historia_usuario) {
        return historiaUsuarioRepository.findById(id_historia_usuario)
                .orElseThrow(() -> new ResourceNotFoundException("HistoriaUsuario", "id_historia_usuario", id_historia_usuario));
    }


    @PutMapping("/historia/{id_historia_usuario}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Historia_usuarioModel updateHistoria(@PathVariable(value = "id_historia_usuario") Long id_historia_usuario,
                           @Valid @RequestBody Historia_usuarioModel historia_usuarioDetails) {

        Historia_usuarioModel historia_usuarioModelModel = historiaUsuarioRepository.findById(id_historia_usuario)
                .orElseThrow(() -> new ResourceNotFoundException("HistoriaUsuario", "id_historia_usuario", id_historia_usuario));

        historia_usuarioModelModel.setCriterio_aceptacion(historia_usuarioDetails.getCriterio_aceptacion());
        historia_usuarioModelModel.setNombre(historia_usuarioDetails.getNombre());
        historia_usuarioModelModel.setFunsionalidad(historia_usuarioDetails.getFunsionalidad());
        historia_usuarioModelModel.setRazon(historia_usuarioDetails.getRazon());
        historia_usuarioModelModel.setRol(historia_usuarioDetails.getRol());
        historia_usuarioModelModel.setSprint(historia_usuarioDetails.getSprint());
        historia_usuarioModelModel.setEncargado(historia_usuarioDetails.getEncargado());
        historia_usuarioModelModel.setComplejidad(historia_usuarioDetails.getComplejidad());
        historia_usuarioModelModel.setPrioridad(historia_usuarioDetails.getPrioridad());
        historia_usuarioModelModel.setEstado(historia_usuarioDetails.getEstado());
        historia_usuarioModelModel.setId_poryect(historia_usuarioDetails.getId_poryect());

        Historia_usuarioModel updatedHistoria_usuario = historiaUsuarioRepository.save(historia_usuarioModelModel);
        return updatedHistoria_usuario;
    }

    
    @DeleteMapping("/historia/{id_historia_usuario}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteHistoria(@PathVariable(value = "id_historia_usuario") Long id_historia_usuario) {
        Historia_usuarioModel historia_usuarioModelModel = historiaUsuarioRepository.findById(id_historia_usuario)
                .orElseThrow(() -> new ResourceNotFoundException("HistoriaUsuario", "id_historia_usuario", id_historia_usuario));

        historiaUsuarioRepository.delete(historia_usuarioModelModel);

        return ResponseEntity.ok().build();
    }
}
