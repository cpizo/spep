package uniajc.edu.spep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.repository.PersonaRepository;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class PersonaController {

    @Autowired
    PersonaRepository personaRepository;

    @GetMapping("/persona")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<PersonaModel> getAllPersona() {
        return personaRepository.findAll();
    }

    @GetMapping("/persona/proyecto/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<PersonaModel> fimdNew(@PathVariable(value = "id") Long Id) {
        return personaRepository.fimdId_poryect(Id);
    }

    @PostMapping("/persona")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public PersonaModel createPersona(@Valid @RequestBody PersonaModel persona) {
        return personaRepository.save(persona);
    }

    @GetMapping("/persona/{id_persona}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public PersonaModel getId(@PathVariable(value = "id_persona") Long Id) {
        return personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));
    }

    @PutMapping("/persona/{id_persona}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public PersonaModel updatePersona(@PathVariable(value = "id_persona") Long Id,
                                           @Valid @RequestBody PersonaModel personaDetails) {

        PersonaModel persona = personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));

        persona.setNombre(personaDetails.getNombre());
        persona.setApellido(personaDetails.getApellido());
        persona.setDireccion(personaDetails.getDireccion());
        persona.setTelefono(personaDetails.getTelefono());
        persona.setValor_hora(personaDetails.getValor_hora());
        persona.setId_poryect(personaDetails.getId_poryect());


        PersonaModel updatedPersona = personaRepository.save(persona);
        return updatedPersona;
    }

    @DeleteMapping("/persona/{id_persona}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deletePersona(@PathVariable(value = "id_persona") Long Id) {
        PersonaModel persona = personaRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException("Persona", "id_persona", Id));

        personaRepository.delete(persona);

        return ResponseEntity.ok().build();
    }
}
