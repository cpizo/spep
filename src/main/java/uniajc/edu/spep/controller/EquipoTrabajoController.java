package uniajc.edu.spep.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import uniajc.edu.spep.exception.ResourceNotFoundException;
import uniajc.edu.spep.model.Equipo_trabajoModel;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.repository.EquipoTrabajoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class EquipoTrabajoController {

    @Autowired
    EquipoTrabajoRepository equipoTrabajoRepository;

    @GetMapping("/equipo")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<Equipo_trabajoModel> getAllEquipo() { return equipoTrabajoRepository.findAll();}

    @GetMapping("/equipo/proyecto/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public List<Equipo_trabajoModel> fimdNew(@PathVariable(value = "id") Long Id) {
        return equipoTrabajoRepository.fimdId_poryect(Id);
    }

    @PostMapping("/equipo")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Equipo_trabajoModel createEquipo(@Valid @RequestBody Equipo_trabajoModel equipo_trabajoModel) {
        return equipoTrabajoRepository.save(equipo_trabajoModel);
    }

    @GetMapping("/equipo/{id_equipo_trabajo}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Equipo_trabajoModel getEquipoById(@PathVariable(value = "id_equipo_trabajo") Long id_equipo_trabajo) {
        return equipoTrabajoRepository.findById(id_equipo_trabajo)
                .orElseThrow(() -> new ResourceNotFoundException("Equipo_trabajo", "id", id_equipo_trabajo));
    }

    @PutMapping("/equipo/{id_equipo_trabajo}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Equipo_trabajoModel updateEquipo(@PathVariable(value = "id_equipo_trabajo") Long id_equipo_trabajo,
                                            @Valid @RequestBody Equipo_trabajoModel equipo_trabajoDetails) {

        Equipo_trabajoModel equipo_trabajoModel = equipoTrabajoRepository.findById(id_equipo_trabajo)
                .orElseThrow(() -> new ResourceNotFoundException("Equipo_trabajo", "id_equipo_trabajo", id_equipo_trabajo));

        equipo_trabajoModel.setId_persona(equipo_trabajoDetails.getId_persona());
        equipo_trabajoModel.setId_rol(equipo_trabajoDetails.getId_rol());
        equipo_trabajoModel.setId_poryect(equipo_trabajoDetails.getId_poryect());

        Equipo_trabajoModel updatedEquipo_trabajo = equipoTrabajoRepository.save(equipo_trabajoModel);
        return updatedEquipo_trabajo;
    }

    @DeleteMapping("/equipo/{id_equipo_trabajo}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteEquipo(@PathVariable(value = "id_equipo_trabajo") Long id_equipo_trabajo) {
        Equipo_trabajoModel equipo_trabajoModel = equipoTrabajoRepository.findById(id_equipo_trabajo)
                .orElseThrow(() -> new ResourceNotFoundException("HistoriaUsuario", "id_equipo_trabajo", id_equipo_trabajo));

        equipoTrabajoRepository.delete(equipo_trabajoModel);

        return ResponseEntity.ok().build();
    }

}
