package uniajc.edu.spep.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "tb_productbacklog")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class ProductbacklogModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_productbacklog;


    private String complejidad;


    private String prioridad;


    private Long id_historia_usuario;

    private Long id_poryect;

    public Long getId_poryect() {
        return id_poryect;
    }

    public void setId_poryect(Long id_poryect) {
        this.id_poryect = id_poryect;
    }

    public Long getId_productbacklog() {
        return id_productbacklog;
    }

    public void setId_productbacklog(Long id_productbacklog) {
        this.id_productbacklog = id_productbacklog;
    }

    public String getComplejidad() {
        return complejidad;
    }

    public void setComplejidad(String complejidad) {
        this.complejidad = complejidad;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public Long getId_historia_usuario() {
        return id_historia_usuario;
    }

    public void setId_historia_usuario(Long id_historia_usuario) {
        this.id_historia_usuario = id_historia_usuario;
    }
}
