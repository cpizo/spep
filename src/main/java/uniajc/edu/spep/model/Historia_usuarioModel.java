package uniajc.edu.spep.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "tb_historia_usuario")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Historia_usuarioModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_historia_usuario;


    private String criterio_aceptacion;


    private String nombre;


    private String funsionalidad;


    private String razon;


    private String rol;


    private String prioridad;


    private String complejidad;

    private String sprint;

    private String estado;

    private Long id_poryect;

    public Long getId_poryect() {
        return id_poryect;
    }

    public void setId_poryect(Long id_poryect) {
        this.id_poryect = id_poryect;
    }

    public Long getId_historia_usuario() {
        return id_historia_usuario;
    }

    public void setId_historia_usuario(Long id_historia_usuario) {
        this.id_historia_usuario = id_historia_usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    private String encargado;

    public String getSprint() {
        return sprint;
    }

    public void setSprint(String sprint) {
        this.sprint = sprint;
    }



    public String getComplejidad() {
        return complejidad;
    }

    public void setComplejidad(String complejidad) {
        this.complejidad = complejidad;
    }
    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public Long getId_Historia_usuario() {
        return id_historia_usuario;
    }

    public void setId_Historia_usuario(Long id_historia_usuario) {
        this.id_historia_usuario = id_historia_usuario;
    }

    public String getCriterio_aceptacion() {
        return criterio_aceptacion;
    }

    public void setCriterio_aceptacion(String criterio_aceptacion) {
        this.criterio_aceptacion = criterio_aceptacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFunsionalidad() {
        return funsionalidad;
    }

    public void setFunsionalidad(String funsionalidad) {
        this.funsionalidad = funsionalidad;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
