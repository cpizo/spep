package uniajc.edu.spep.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "tb_proyecto")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class ProyectoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_proyecto;


    private String duracion;


    private Long id_equipo_trabajo;


    private String historia_max;


    private String historia_min;


    private String nombre;


    private String peso;


    private String pivote;


    private String tiempo;

    private String fecha_inicio;

    private String correo;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    private Long id_poryect;

    public Long getId_poryect() {
        return id_poryect;
    }

    public void setId_poryect(Long id_poryect) {
        this.id_poryect = id_poryect;
    }

    public int getCant_sprint() {
        return cant_sprint;
    }

    public void setCant_sprint(int cant_sprint) {
        this.cant_sprint = cant_sprint;
    }

    private int cant_sprint;

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getCompromiso() {
        return compromiso;
    }

    public void setCompromiso(String compromiso) {
        this.compromiso = compromiso;
    }

    private String compromiso;


    public Long getId_equipo_trabajo() {
        return id_equipo_trabajo;
    }

    public void setId_equipo_trabajo(Long id_equipo_trabajo) {
        this.id_equipo_trabajo = id_equipo_trabajo;
    }

    public Long getId_proyecto() {
        return id_proyecto;
    }

    public void setId_proyecto(Long id_proyecto) {
        this.id_proyecto = id_proyecto;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }



    public String getHistoria_max() {
        return historia_max;
    }

    public void setHistoria_max(String historia_max) {
        this.historia_max = historia_max;
    }

    public String getHistoria_min() {
        return historia_min;
    }

    public void setHistoria_min(String historia_min) {
        this.historia_min = historia_min;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getPivote() {
        return pivote;
    }

    public void setPivote(String pivote) {
        this.pivote = pivote;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }
}
