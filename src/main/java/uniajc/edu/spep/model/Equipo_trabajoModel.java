package uniajc.edu.spep.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "tb_equipo_trabajo")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Equipo_trabajoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_equipo_trabajo;

    private Long id_poryect;

    public Long getId_poryect() {
        return id_poryect;
    }

    public void setId_poryect(Long id_poryect) {
        this.id_poryect = id_poryect;
    }

    private Long id_persona;


    private Long id_rol;

    public Long getId_equipo_trabajo() {
        return id_equipo_trabajo;
    }

    public void setId_equipo_trabajo(Long id_equipo_trabajo) {
        this.id_equipo_trabajo = id_equipo_trabajo;
    }

    public Long getId_persona() {
        return id_persona;
    }

    public void setId_persona(Long id_persona) {
        this.id_persona = id_persona;
    }

    public Long getId_rol() {
        return id_rol;
    }

    public void setId_rol(Long id_rol) {
        this.id_rol = id_rol;
    }

}
