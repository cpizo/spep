package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.model.ProductbacklogModel;

import java.util.List;


@Repository
public interface ProducbacklogRepository extends JpaRepository<ProductbacklogModel, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM tb_productbacklog WHERE id_poryect= :id")
    List<ProductbacklogModel> fimdId_poryect (@Param(value = "id") Long id);

}
