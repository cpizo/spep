package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.Historia_usuarioModel;
import uniajc.edu.spep.model.PersonaModel;

import java.util.List;


@Repository
public interface HistoriaUsuarioRepository extends JpaRepository<Historia_usuarioModel, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM tb_historia_usuario WHERE id_poryect= :id")
    List<Historia_usuarioModel> fimdId_poryect (@Param(value = "id") Long id);
}
