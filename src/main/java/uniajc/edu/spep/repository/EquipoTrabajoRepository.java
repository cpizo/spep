package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.Equipo_trabajoModel;
import uniajc.edu.spep.model.PersonaModel;

import java.util.List;


@Repository
public interface EquipoTrabajoRepository extends JpaRepository<Equipo_trabajoModel, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM tb_equipo_trabajo WHERE id_poryect= :id")
    List<Equipo_trabajoModel> fimdId_poryect (@Param(value = "id") Long id);

}
