package uniajc.edu.spep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uniajc.edu.spep.model.PersonaModel;
import uniajc.edu.spep.model.ProyectoModel;

import java.util.List;


@Repository
public interface ProyectoRepository extends JpaRepository<ProyectoModel, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM tb_proyecto WHERE id_poryect= :id")
    List<ProyectoModel> fimdId_poryect (@Param(value = "id") Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM tb_proyecto WHERE correo= :id")
    List<ProyectoModel> fimdNameProyect (@Param(value = "id") String id);
}
